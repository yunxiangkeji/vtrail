//@json
function main(tbl){
	let cnd=Cnd.where("1","=",1)
	if(!isEmpty(tbl.trail_id)){
		cnd.and("trail_id","=",tbl.trail_id)
	}
	let page=tbl.pageNum||1;
	let limit=tbl.pageSize||10;
	return dao.table("trail_device",cnd.asc("id"),page,limit)              
}