//@json
function main(tbl){
	tbl.put("table","trail_device")
	//1、在超脑中注册设备（https://api2.hik-cloud.com/api/v1/open/basic/devices/create）
	let HttpHelper = Java.type("api.HttpHelper")
	let httpHelper = new HttpHelper()
	let device_info = { 
			 "deviceSerial": tbl.device_code,
			 "groupNo": "7260", 
			 "validateCode": "videa7260" 
			}
	device_info = httpHelper.post("https://api2.hik-cloud.com/api/v1/open/basic/devices/create",device_info)
	//验证序列号
	if(device_info.code != 200)
		return {code:404,msg:"设备注册异常："+device_info.message}
	//获取设备信息
	print("服务器端注册超脑===>>>" + device_info)
	//2、获取设备id值
	tbl.device_id = device_info.data.deviceId
	
	//3、创建人脸库（https://api2.hik-cloud.com/api/v1/community/superBrains/faceLibs）
	tbl.device_face_libid = uuid();
	let faceLibs = {
		"deviceId": device_info.data.deviceId,
	    "faceLibs": [{
	        "index": 1,
	        "faceLibName": "videa",
	        "thresholdValue": 75,
	        "customFaceLibId": tbl.device_face_libid
	   }]
	}
	
	faceLibs = httpHelper.post("https://api2.hik-cloud.com/api/v1/community/superBrains/faceLibs",faceLibs)
	print("创建本地人脸库===>>>" + faceLibs)
	if(faceLibs.code != 200){
		//操作失败，则在服务器端注销掉超脑
		let res = httpHelper.post("https://api2.hik-cloud.com/api/v1/open/basic/devices/delete?deviceSerial=" + tbl.device_code,null);
		print("超脑注销中===>>>" + res)	
		return {code:404,msg:"创建本地人脸库异常："+faceLibs.message}
	}
		
	try {
		$mager(tbl)
		return {code:0,msg:"更新成功!"}
	} catch (e) {
		return {code:404,msg:"更新失败!"}
	}
}