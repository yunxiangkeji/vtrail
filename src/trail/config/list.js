//@json
function main(tbl){
	let cnd=Cnd.where("1","=",1)
	if(!isEmpty(tbl.name)){
		cnd.and("name","like",tbl.name+"%")
	}
	if(!isEmpty(tbl.status)){
		cnd.and("status","=",tbl.status)
	}
	let page=tbl.pageNum||1;
	let limit=tbl.pageSize||10;
	return dao.table("trail_config",cnd.desc("id"),page,limit)              
}