//@json
function main(tbl){
	let browser=Devices.browser(req());//浏览器
	let os=Devices.os(req());//浏览器
	let admin=dao.fetch("sys_user",Cnd.where("username","=",tbl.username).and("password","=",tbl.password))
	let data={"table":"sys_login","ip":ip(),"name":tbl.username,browser:browser,os:os,status:0}
	if(admin!=null){
		if(admin.role_id>0){
			let role=dao.fetch("sys_role",Cnd.where("id","=",admin.role_id))
			if(role.status==0){
				data.msg="该角色已停用";
				sdata(data);
				return {code:-2,msg:"该角色已停用"};
			}
		}
		if(admin.status==0){
			data.msg="该用户已停用";
			sdata(data);
			return {code:-2,msg:"该用户已停用"};
		}
		sattr("admin",admin);
		data.msg="登录成功";
		data.status=1;
		sdata(data);
		if(isEmpty(admin.get("code"))){
			sattr("unit",{name:"维艾狄尔智慧步道",corp:"北京维艾狄尔信息科技有限公司",ico:"/favicon.ico",logo:"/logo.png"});
		}else{
			sattr("unit",dao.fetch("sys_unit",Cnd.where("code","=",admin.get("code"))));
		}
		return {code:0,msg:"登录成功"}
	}else{
		data.msg="用户名或密码不正确";
		sdata(data);
		return {code:-1,msg:"用户名或密码不正确"};
	}
}

function sdata(d){
	$save(d)
}