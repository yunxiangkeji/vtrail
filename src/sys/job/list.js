//@json
function main(tbl){
	let cnd=Cnd.where("1","=",1)
	if(!isEmpty(tbl.title)){
		cnd.and("u.title","like",tbl.title+"%")
	}
	if(!isEmpty(tbl.id)){
		cnd.and("u.id","=",tbl.id)
	}
	if(!isEmpty(tbl.code)){
		cnd.and("u.code","like",tbl.code+"%")
	}
	if(!isEmpty(tbl.name)){
		cnd.and("u.name","like",tbl.name+"%")
	}
	if(!isEmpty(tbl.phone)){
		cnd.and("u.phone","like",tbl.phone+"%")
	}
	if(!isEmpty(tbl.sbdj)){
		cnd.and("u.sbdj","=",tbl.sbdj)
	}
	if(!isEmpty(tbl.card)){
		cnd.and("u.card","like",tbl.card+"%")
	}
	if(!isEmpty(tbl.add_time)){
		cnd.and("u.add_time",">",tbl.add_time[0]+" 00:00:00")
		.and("u.add_time","<",tbl.add_time[1]+" 23:59:59")
	}
	if(!isEmpty(tbl.yq)){
		if(tbl.yq=="延期申请"){
			cnd.and("u.yq <> ''")
		}else{
			cnd.and("u.yq","=","")
		}
	}
	if(!isEmpty(tbl.lw)){
		let lw=parseInt(tbl.lw);
		if(lw==60){
			cnd.and("u.lw",">",59)
		}else if(lw==59){
			cnd.and("u.lw","<",60).and("u.lw",">",0)
		}else
		cnd.and("u.lw","=",lw)
	}
	if(!isEmpty(tbl.course_id)){
		if(tbl.course_id=="1"){
			cnd.and("u.course_id",">",0)
		}else{
			cnd.and("u.course_id","=",0)
		}
	}
	let page=tbl.page||1;
	let limit=tbl.limit||20;
	let table="sys_job u";
	var pager=dao.table(table,cnd.desc("id"),page,limit)
	return pager;
}