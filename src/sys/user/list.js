//@json
function main(tbl){
	let cnd=Cnd.where("role_id",">",1)
	if(!isEmpty(tbl.id)){
		cnd.and("id","=",tbl.id)
	}
	if(!isEmpty(tbl.code)){
		cnd.and("code","like","%"+tbl.code+"%")
	}
	let page=tbl.pageNum||1;
	let limit=tbl.pageSize||10;
	return dao.table("sys_user",cnd.desc("id"),page,limit)              
}