//@json
function main(tbl){
	let cnd=Cnd.where("1","=",1)
	if(tbl.unit_id){
		cnd.and("unit_id","=",tbl.unit_id)
	}
	if(!isEmpty(tbl.id)){
		cnd.and("u.id","=",tbl.id)
	}
	let page=tbl.pageNum||1;
	let limit=tbl.pageSize||10;
	return dao.table("sys_config",cnd.desc("id"),page,limit)              
}