//@json
function main(tbl){
	let cnd=Cnd.where("1","=",1)
	if(tbl.unit_id){
		cnd.and("unit_id","=",tbl.unit_id)
	}
	if(!isEmpty(tbl.id)){
		cnd.and("u.id","=",tbl.id)
	}
	let page=tbl.page||1;
	let limit=tbl.limit||20;
	return dao.table("sys_log",cnd.desc("id"),page,limit)              
}