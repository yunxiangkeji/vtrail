//@json
function main(tbl){
	let cnd=Cnd.where("1","=",1)
	if(tbl.unit_id){
		cnd.and("unit_id","=",tbl.unit_id)
	}
	if(!isEmpty(tbl.id)){
		cnd.and("u.id","=",tbl.id)
	}
	if(!isEmpty(tbl.name)){
		cnd.and("name","like",tbl.name+"%")
	}
	if(!isEmpty(tbl.ip)){
		cnd.and("ip","like",tbl.ip+"%")
	}
	if(!isEmpty(tbl.status)){
		cnd.and("status","=",tbl.status)
	}
	if(tbl.add_time[0]&&tbl.add_time[1]){
		cnd.and("add_time",">=",tbl.add_time[0]+" 00:00:00")
		cnd.and("add_time","<=",tbl.add_time[1]+" 23:59:59")
	}
	let page=tbl.pageNum||1;
	let limit=tbl.pageSize||20;
	return dao.table("sys_login",cnd.desc("id"),page,limit)
}