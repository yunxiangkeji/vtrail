//@json
function main(tbl){
	var menuIds=tbl.remove("menuids");
	tbl.put("table","sys_role")
	try {
		if(!isEmpty(tbl.id)){
			dao.update(tbl)
		}else{
			tbl.remove("id")
			tbl.put("+id",0)
			tbl=dao.insert(tbl);
		}
		var role_id=tbl.id;
		if(!Lang.isEmpty(menuIds)){
			dao.delete("sys_role_menu",Cnd.where("role_id","=",tbl.id));
			var srms=new ArrayList();
			for (var i = 0; i < menuIds.size(); i++) {
				var srm={"table":"sys_role_menu","role_id":role_id,"menu_id":menuIds[i]};
				srms.add(srm);
			}
			dao.insert(srms);
		}
		return {code:0,msg:"更新成功!"}
	} catch (e) {
		return {code:404,msg:"更新失败!"}
	}
}