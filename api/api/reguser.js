//@json
function main(tbl){
	if(tbl.trail_id == null)
		return{code:-1,msg:"trail_id 不能为空！！！"}
	print(tbl)
	tbl.put("table","trail_user")
	//注册人脸到海康威视后台人脸库（https://api2.hik-cloud.com/api/v1/community/superBrains/persons）
	tbl.userface_id = uuid()
	let HttpHelper = Java.type("api.HttpHelper")
	let httpHelper = new HttpHelper()
	print("D:\\tomcat\\webapps\\"+tbl.userface)
	let base64 = httpHelper.encodeBase64File("D:\\tomcat\\webapps\\"+tbl.userface)
	if(null == base64)
		return {code:-1,msg:"照片转换base64失败"}
	
	let persons ={
    "employeeNo": tbl.userface_id ,        
    "personName": tbl.username,    
    "face": {
			"faceName" : tbl.userface_id,
			"faceData" : base64
		}
	}
	
	//上传人脸到海康云眸（https://api2.hik-cloud.com/api/v1/community/superBrains/persons）
	persons = httpHelper.post("https://api2.hik-cloud.com/api/v1/community/superBrains/persons",persons)
	if(persons.code!=200)
		return {code:-1,msg:"人脸上传错误："+persons.message}
	
	//下发人脸到设备（https://api2.hik-cloud.com/api/v1/community/superBrains/actions/asyncDeliveredFaces）
	//1、查询该步道的所有设备
	let devices = dao.query("trail_device", Cnd.where("trail_id", "=", tbl.trail_id));
	//循环下发人脸库
	for(let i = 0 ; i < devices.length ; i++){
		let asyncDeliveredFaces = {
			"deviceId": devices[i].device_id,
			"customFaceLibId":devices[i].device_face_libid,
			"employeeNoList":[tbl.userface_id]
		}
		asyncDeliveredFaces = httpHelper.post("https://api2.hik-cloud.com/api/v1/community/superBrains/actions/asyncDeliveredFaces",asyncDeliveredFaces)
		print(asyncDeliveredFaces)
		if(asyncDeliveredFaces.code != 200){
			return {code:-1,msg:"====>>>>下发人脸照片错误："+asyncDeliveredFaces.message}
		}
	}
	
	try {
		$mager(tbl)
		let userinfo = dao.fetch("trail_user",Cnd.where("userface_id","=",tbl.userface_id))
		return {code:0,data:userinfo,msg:"更新成功!"}
	} catch (e) {
		return {code:-1,msg:"更新失败!"}
	}
}