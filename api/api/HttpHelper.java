package api;

import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import com.jse.http.HttpConnection;
import com.jse.json.JSONObject;
import com.jse.json.Json;
import com.jse.util.Http;

/**
 * 定义一个访问海康威视接口的类，此类为静态类，相当于单例模式
 * 
 * @author X1 C
 *
 */
public class HttpHelper {
	
	// 海康接口返回的token，过期后需要重新获取
	public static String token = "bearer 4c6713a8-7a95-49b8-af2f-9f64b9a2582c";
	//消费者id用于消息队列消息读取
	public static String consumerId = null;

	/**
	 * 重新获取token
	 */
	public static void getToken() {
		JSONObject data = (JSONObject) Json.fromJson(Http.post("https://api2.hik-cloud.com/oauth/token",
				"client_id=319516e494a14bf98ec467af487653d3&client_secret=efbe0d25d6cd47dab7e5c903f48a4f13&grant_type=client_credentials"));
		token = "bearer " + data.get("access_token").toString();
		System.out.println("重新申请token========>>>>>>>" + token);
	}
	
	public String getConsumerId() {
		if(null == consumerId) {
			HttpHelper helper = new HttpHelper();
			JSONObject data = (JSONObject)helper.post("https://api2.hik-cloud.com/api/v1/mq/consumer/group1", null);
			if(data.getIntValue("code") == 200)
				consumerId = data.getJSONObject("data").getString("consumerId");
			else
				System.out.println("获取消息队列id错误"+data.toJSONString());
		}
		return consumerId;
	}

//"https://api2.hik-cloud.com/api/v1/community/superBrains/persons" 注册人脸到平台上
//"https://api2.hik-cloud.com/api/v1/community/superBrains/actions/asyncDeliveredFaces"将人脸信息下发到设备上

	/**
	 * 将文件转成base64 字符串
	 * @param path 文件路径
	 */
	public String encodeBase64File(String path) {
		File file = new File(path);
		String base64 = null;
		try {
			FileInputStream inputFile = new FileInputStream(file);
			byte[] buffer = new byte[(int) file.length()];
			inputFile.read(buffer);
			inputFile.close();
			base64 = Base64.getEncoder().encodeToString(buffer);
		}catch (Exception e) {
			base64 = null;
		}
			return base64;
	}

	
	public static void testasynface() {
		//https://api2.hik-cloud.com/api/v1/community/superBrains/actions/asyncDeliveredFaces
		//		deviceId	设备ID	String	32	是
		//		customFaceLibId	自定义人脸库ID	String	32	是
		//		employeeNoList	自定义人员ID列表	List	1000	是
		if(null == token)
			getToken();
		
		JSONObject param = new JSONObject();
		param.put("deviceId", "611e5e7e803240bd888475a8cb266eea");
		param.put("customFaceLibId","F1A372DE0B4F4CC5AA92BBA6236024E2");
		param.put("employeeNoList",new String[] {"1"});
		
		Map<String, String> header = new HashMap<String, String>();
		header.put("Content-Type", "application/json");
		header.put("Authorization", token);
		System.out.println(param);
		JSONObject data = (JSONObject) Json.fromJson(Http.post("https://api2.hik-cloud.com/api/v1/community/superBrains/actions/asyncDeliveredFaces",header,param.toJSONString(),2000,2000));
		System.out.println(data);
	}
	
	public Object post(String url,Map param) {
			
		Map<String, String> header = new HashMap<String, String>();
		header.put("Content-Type", "application/json;charset=UTF-8");
		header.put("Authorization", token);
		
		JSONObject data = (JSONObject) Json.fromJson(Http.post(url,header,Json.toJson(param),2000,2000));
		if("invalid_token".equals(data.getString("error"))) {
			System.out.println("token 过期了");
			getToken();
			//申请新的token，然后做一个递归
			return post(url,param);
		}
		return data;
	}

	public Object post(String url,String content_type,Map param) {
		
		Map<String, String> header = new HashMap<String, String>();
		header.put("Content-Type", content_type);
		header.put("Authorization", token);
		
		JSONObject data = (JSONObject) Json.fromJson(Http.post(url,header,Json.toJson(param),15000,15000));
		if("invalid_token".equals(data.getString("error"))) {
			System.out.println("token 过期了");
			getToken();
			//申请新的token，然后做一个递归
			return post(url,param);
		}
		return data;
	}
	
	
	public static void main(String[] args) throws Exception {
		 URL url =new URL("http://localhost/api/upload");
		 HttpURLConnection conn = (HttpURLConnection)url.openConnection();
		 conn.setDoInput(true);
		 conn.setDoOutput(true);
		 
		 conn.setUseCaches(false);
		 conn.setRequestMethod("POST");
		 
		
	}

}
