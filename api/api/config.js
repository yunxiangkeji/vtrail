//@json
function main(tbl) {
	
	var rankings = dao.fetch("trail_rankings", Cnd.where("imei", "=", tbl.imei))
	if (rankings != null) {
		var trailinfo = dao.fetch("trail_info", Cnd.where("id", "=",rankings.trail_id))
		rankings.remove('id')
		rankings.remove('imei')
		rankings.remove('site')
		rankings.remove('static')
		rankings.remove('add_time')
		rankings.remove('update_time')
		rankings.add("trail_title", trailinfo.title)
		rankings.add("logo", trailinfo.logo)
		rankings.add("copyright", trailinfo.copyright)
		rankings.add("province", trailinfo.province)
		rankings.add("city", trailinfo.city)
		rankings.add("county", trailinfo.county)
		return {code : 0,data : rankings}
	}
	return {code : -1}
}