//@json
function main(tbl){
	let cnd=Cnd.where("code","=",tbl.code)
	if(!isEmpty(tbl.lasttime)){
		cnd.and("update_time",">",tbl.lasttime)
	}
	let page=tbl.pageNum||1;
	let limit=tbl.pageSize||20;
	userlist = dao.table("trail_user",cnd.asc("id"),page,limit) 
	userlist.remove('pager')
	return userlist
}