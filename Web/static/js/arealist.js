$("#arealist").html("<input type=\"hidden\" name=\"province\" id=\"province\" />\n" +
		"		                 <input type=\"hidden\" name=\"city\" id=\"city\" />\n" +
		"		                 <input type=\"hidden\" name=\"county\" id=\"county\" />\n" +
		"		                 <input type=\"hidden\" name=\"towntr\" id=\"towntr\" />\n" +
		"		                 <input type=\"hidden\" name=\"villagetr\" id=\"villagetr\" />\n" +
		"								地区：<select id=\"area0\" onchange=\"arealist(this.value)\" ><option\n" +
		"											value=\"0\">请选择</option>\n" +
		"										<option value=\"11\">北京市</option>\n" +
		"										<option value=\"12\">天津市</option>\n" +
		"										<option value=\"13\">河北省</option>\n" +
		"										<option value=\"14\">山西省</option>\n" +
		"										<option value=\"15\">内蒙古自治区</option>\n" +
		"										<option value=\"21\">辽宁省</option>\n" +
		"										<option value=\"22\">吉林省</option>\n" +
		"										<option value=\"23\">黑龙江省</option>\n" +
		"										<option value=\"31\">上海市</option>\n" +
		"										<option value=\"32\">江苏省</option>\n" +
		"										<option value=\"33\">浙江省</option>\n" +
		"										<option value=\"34\">安徽省</option>\n" +
		"										<option value=\"35\">福建省</option>\n" +
		"										<option value=\"36\">江西省</option>\n" +
		"										<option value=\"37\">山东省</option>\n" +
		"										<option value=\"41\">河南省</option>\n" +
		"										<option value=\"42\">湖北省</option>\n" +
		"										<option value=\"43\">湖南省</option>\n" +
		"										<option value=\"44\">广东省</option>\n" +
		"										<option value=\"45\">广西壮族自治区</option>\n" +
		"										<option value=\"46\">海南省</option>\n" +
		"										<option value=\"50\">重庆市</option>\n" +
		"										<option value=\"51\">四川省</option>\n" +
		"										<option value=\"52\">贵州省</option>\n" +
		"										<option value=\"53\">云南省</option>\n" +
		"										<option value=\"54\">西藏自治区</option>\n" +
		"										<option value=\"61\">陕西省</option>\n" +
		"										<option value=\"62\">甘肃省</option>\n" +
		"										<option value=\"63\">青海省</option>\n" +
		"										<option value=\"64\">宁夏回族自治区</option>\n" +
		"										<option value=\"65\">新疆维吾尔自治区</option>\n" +
		"										<option value=\"台湾省\">台湾省</option>\n" +
		"										<option value=\"香港特别行政区\">香港特别行政区</option>\n" +
		"										<option value=\"澳门特别行政区\">澳门特别行政区</option>\n" +
		"									</select> \n" +
		"									<select style=\"display: none;\" id=\"area1\" onchange=\"arealist(this.value)\" >\n" +
		"										</select>\n" +
		"									<select style=\"display: none;\" id=\"area2\" onchange=\"arealist(this.value)\" >\n" +
		"										</select>\n" +
		"									<select style=\"display: none;\" id=\"area3\" onchange=\"arealist(this.value)\" >\n" +
		"										</select>\n" +
		"									<select style=\"display: none;\" id=\"area4\" onchange=\"arealist(this.value)\" >\n" +
		"									</select>")
function areacode(){
	if($("#area4 option:selected").val()!=""){
		return $("#area4 option:selected").val()
	}else if($("#area3 option:selected").val()!=""){
		return $("#area3 option:selected").val()
	}else if($("#area2 option:selected").val()!=""){
		return $("#area2 option:selected").val()
	}else if($("#area1 option:selected").val()!=""){
		return $("#area1 option:selected").val()
	}else if($("#area0 option:selected").val()!=""){
		return $("#area0 option:selected").val()
	}
	return "";
}
		function arealist(code){
	fetch(ctx+'api/area/list?code='+code)
    .then(res => res.json())
    .then(json => {
    	var data=json.data;
    	if(data){
    		if(code.length==2){//2
    			$("#area1").html("<option value=''>请选择</option>")
    			$("#area2").html("")
    			$("#area2").hide();
    			$("#area3").html("")
    			$("#area3").hide();
    			$("#area4").html("")
    			$("#area4").hide();
    			for (var d of data) {
    				$("#area1").append("<option value='"+d.areacode+"'>"+d.title+"</option>")
				}
    			$("#area1").show();
    			$("#province").val($('#area0 option:selected').text());
    			$("#city").val("");
    			$("#county").val("");
    			$("#towntr").val("");
    			$("#villagetr").val("");
    		}else if(code.length==4){//3
    			$("#city").val($('#area1 option:selected').text());
    			$("#county").val("");
    			$("#towntr").val("");
    			$("#villagetr").val("");
    			if($("#arealist").attr("level")=="2"){
    				return;
    			}
    			$("#area2").html("<option value=''>请选择</option>")
    			$("#area3").html("")
    			$("#area3").hide();
    			$("#area4").html("")
    			$("#area4").hide();
    			for (var d of data) {
    				$("#area2").append("<option value='"+d.areacode+"'>"+d.title+"</option>")
				}
    			$("#area2").show();
    			
    		}else if(code.length==6){//4
    			$("#county").val($('#area2 option:selected').text());
    			$("#towntr").val("");
    			$("#villagetr").val("");
    			if($("#arealist").attr("level")=="3"){
    				return;
    			}
    			$("#area3").html("<option value=''>请选择</option>")
    			$("#area4").html("")
    			$("#area4").hide();
    			for (var d of data) {
    				$("#area3").append("<option value='"+d.areacode+"'>"+d.title+"</option>")
				}
    			$("#area3").show();
    			
    		}else if(code.length==9){//5
    			$("#towntr").val($('#area3 option:selected').text());
    			$("#villagetr").val("");
    			if($("#arealist").attr("level")=="4"){
    				return;
    			}
    			$("#area4").html("<option value=''>请选择</option>")
    			for (var d of data) {
    				$("#area4").append("<option value='"+d.areacode+"'>"+d.title+"</option>")
				}
    			$("#area4").show();
    			
    		}else if(code.length==12){//5
    			$("#villagetr").val($('#area4 option:selected').text());
    		}else{
    			$("#area1").html("");
    			$("#area1").hide();
    			$("#area2").html("")
    			$("#area2").hide();
    			$("#area3").html("")
    			$("#area3").hide();
    			$("#area4").html("")
    			$("#area4").hide();
    			$("#province").val("");
    			$("#city").val("");
    			$("#county").val("");
    			$("#towntr").val("");
    			$("#villagetr").val("");
    		}
    	}
    })
}