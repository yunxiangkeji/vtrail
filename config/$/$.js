function nots(){
	return Arrays.asList("/login","/user/login","/logout","/notauth","/captcha/4","/api/up","/captcha/id")
}

function main(req,resp){
		if(req.servletPath.startsWith("/api/")){
			return null;
		}
		let u=req.session.getAttribute("admin");
		if(u==null){
			return ">>:/login";
		}else{
			let role_ids=u.role_id;//Arrays.asList(u.role_id);//角色id集合 多种角色可根据用户id与角色关联扩展查询出来
			req.setAttribute("_view","th");
			if(role_ids==1){//超级管理员自动过滤
				return null;
			}else{
				if(isEmpty(u.auths)){
					let auths0=Arrays.asList("index","main/");//默认权限
					let auths=dao.select(`select perms from sys_menu where id in (
		            		select menu_id from sys_role_menu where role_id = ${role_ids})`,"set");
					if(auths!=null){
						auths.addAll(auths0)
					}
					u.put("auths",auths);
					req.session.setAttribute("admin",u);
				}
				if(!Auth.has(req.getServletPath(),u.auths)){//需要权限
					return ">>:/notauth"
				}
			}
		}
}